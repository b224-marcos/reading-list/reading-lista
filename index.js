//1.

function makeAlphabet (str) {
	newArr = str.split('').sort().join('')
	return newArr
};

letStrResult = makeAlphabet("webmaster");
console.log(letStrResult);


//2.
function countVowel (str) {
	let vowelsCount = 0;
	let vowels = ['a', 'e', 'i', 'o', 'u']
	for (let char of str) {
		if (vowels.includes(char)) {
			vowelsCount++
		}
	}
	return vowelsCount
};

let countVowelResult = countVowel("The quick brown fox");
console.log(countVowelResult)



//3.

let countries = ["Philippines", "Indonesia", "Vietnam", "Thailand"]



function addCountries (country) {
	let countriesArray = countries.unshift(country);
	let sortCountries = countries.sort()
	console.log(sortCountries)

	
};

addCountries("Singapore")
addCountries("Cambodia")
addCountries("Myanmar")


//4. 

let person = {
	firstname: "Jason",
	lastName: "Swift",
	age: 25,
	gender: "male",
	nationality: "Canadian"
};
console.log(person)


//5.

function Person (firstname, lastName, age, gender, nationality, introduce) {
		this.firstname = firstname
		this.lastName = lastName
		this.age = age
		this.gender = gender
		this.nationality = nationality
		this.introduce = function() {
			console.log(`Hi my name is ${this.firstname} ${this.lastName}, ${this.age} year old ${this.nationality}, at your service`)
		}
	};

let person1 = new Person ("Brad", "Pitt", 55, "male", "American");
console.log(person1)
console.log(person1.introduce());


let person2 = new Person ("Jennifer", "Lopez", 54, "female", "Mexican");
console.log(person2)
console.log(person2.introduce());

let person3 = new Person ("Jose", "Gomez", 98, "male", "Filipino");
console.log(person3)
console.log(person3.introduce());



